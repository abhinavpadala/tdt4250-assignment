# TDT4250 Assignment

Assignment for [TDT4250](https://www.ntnu.no/studier/emner/TDT4250) course at NTNU.


## Structure

### StudyProgramModel 
Contains model (**/model/**) and generated source codes (**/src/**). 
Model is represented by **studyProgram.ecore** file.

### StudyProgramModel.edit
TODO

### StudyProgramModel.editor
TODO 

### StudyProgramModel.test
TODO
