/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.Semester#getSemesterType <em>Semester Type</em>}</li>
 *   <li>{@link model.Semester#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link model.Semester#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link model.Semester#getElectiveCourseGroups <em>Elective Course Groups</em>}</li>
 *   <li>{@link model.Semester#getAllCourses <em>All Courses</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getSemester()
 * @model
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester Type</b></em>' attribute.
	 * The literals are from the enumeration {@link model.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Type</em>' attribute.
	 * @see model.SemesterType
	 * @see #setSemesterType(SemesterType)
	 * @see model.ModelPackage#getSemester_SemesterType()
	 * @model
	 * @generated
	 */
	SemesterType getSemesterType();

	/**
	 * Sets the value of the '{@link model.Semester#getSemesterType <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester Type</em>' attribute.
	 * @see model.SemesterType
	 * @see #getSemesterType()
	 * @generated
	 */
	void setSemesterType(SemesterType value);

	/**
	 * Returns the value of the '<em><b>Mandatory Courses</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory Courses</em>' reference.
	 * @see #setMandatoryCourses(Course)
	 * @see model.ModelPackage#getSemester_MandatoryCourses()
	 * @model
	 * @generated
	 */
	Course getMandatoryCourses();

	/**
	 * Sets the value of the '{@link model.Semester#getMandatoryCourses <em>Mandatory Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory Courses</em>' reference.
	 * @see #getMandatoryCourses()
	 * @generated
	 */
	void setMandatoryCourses(Course value);

	/**
	 * Returns the value of the '<em><b>Elective Courses</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Courses</em>' reference.
	 * @see #setElectiveCourses(Course)
	 * @see model.ModelPackage#getSemester_ElectiveCourses()
	 * @model
	 * @generated
	 */
	Course getElectiveCourses();

	/**
	 * Sets the value of the '{@link model.Semester#getElectiveCourses <em>Elective Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elective Courses</em>' reference.
	 * @see #getElectiveCourses()
	 * @generated
	 */
	void setElectiveCourses(Course value);

	/**
	 * Returns the value of the '<em><b>Elective Course Groups</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Course Groups</em>' containment reference.
	 * @see #setElectiveCourseGroups(ElectiveGroup)
	 * @see model.ModelPackage#getSemester_ElectiveCourseGroups()
	 * @model containment="true"
	 * @generated
	 */
	ElectiveGroup getElectiveCourseGroups();

	/**
	 * Sets the value of the '{@link model.Semester#getElectiveCourseGroups <em>Elective Course Groups</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elective Course Groups</em>' containment reference.
	 * @see #getElectiveCourseGroups()
	 * @generated
	 */
	void setElectiveCourseGroups(ElectiveGroup value);

	/**
	 * Returns the value of the '<em><b>All Courses</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Courses</em>' reference.
	 * @see #setAllCourses(Course)
	 * @see model.ModelPackage#getSemester_AllCourses()
	 * @model
	 * @generated
	 */
	Course getAllCourses();

	/**
	 * Sets the value of the '{@link model.Semester#getAllCourses <em>All Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Courses</em>' reference.
	 * @see #getAllCourses()
	 * @generated
	 */
	void setAllCourses(Course value);

} // Semester
