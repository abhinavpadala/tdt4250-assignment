/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.University#getCatalogue <em>Catalogue</em>}</li>
 *   <li>{@link model.University#getPrograms <em>Programs</em>}</li>
 *   <li>{@link model.University#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends EObject {
	/**
	 * Returns the value of the '<em><b>Catalogue</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catalogue</em>' containment reference.
	 * @see #setCatalogue(CourseCatalogue)
	 * @see model.ModelPackage#getUniversity_Catalogue()
	 * @model containment="true"
	 * @generated
	 */
	CourseCatalogue getCatalogue();

	/**
	 * Sets the value of the '{@link model.University#getCatalogue <em>Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catalogue</em>' containment reference.
	 * @see #getCatalogue()
	 * @generated
	 */
	void setCatalogue(CourseCatalogue value);

	/**
	 * Returns the value of the '<em><b>Programs</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link model.Program#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programs</em>' containment reference.
	 * @see #setPrograms(Program)
	 * @see model.ModelPackage#getUniversity_Programs()
	 * @see model.Program#getUniversity
	 * @model opposite="university" containment="true"
	 * @generated
	 */
	Program getPrograms();

	/**
	 * Sets the value of the '{@link model.University#getPrograms <em>Programs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Programs</em>' containment reference.
	 * @see #getPrograms()
	 * @generated
	 */
	void setPrograms(Program value);

	/**
	 * Returns the value of the '<em><b>Specialisation Catalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisation Catalogue</em>' reference.
	 * @see #setSpecialisationCatalogue(SpecialisationCatalogue)
	 * @see model.ModelPackage#getUniversity_SpecialisationCatalogue()
	 * @model
	 * @generated
	 */
	SpecialisationCatalogue getSpecialisationCatalogue();

	/**
	 * Sets the value of the '{@link model.University#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialisation Catalogue</em>' reference.
	 * @see #getSpecialisationCatalogue()
	 * @generated
	 */
	void setSpecialisationCatalogue(SpecialisationCatalogue value);

} // University
