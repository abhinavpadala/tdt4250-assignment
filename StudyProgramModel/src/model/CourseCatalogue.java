/**
 */
package model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Catalogue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.CourseCatalogue#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getCourseCatalogue()
 * @model
 * @generated
 */
public interface CourseCatalogue extends EObject {
	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see model.ModelPackage#getCourseCatalogue_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

} // CourseCatalogue
