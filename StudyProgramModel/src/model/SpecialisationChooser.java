/**
 */
package model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation Chooser</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.SpecialisationChooser#getAvailableSpecialisations <em>Available Specialisations</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getSpecialisationChooser()
 * @model
 * @generated
 */
public interface SpecialisationChooser extends ProgramUnit {
	/**
	 * Returns the value of the '<em><b>Available Specialisations</b></em>' reference list.
	 * The list contents are of type {@link model.Specialisation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Specialisations</em>' reference list.
	 * @see model.ModelPackage#getSpecialisationChooser_AvailableSpecialisations()
	 * @model
	 * @generated
	 */
	EList<Specialisation> getAvailableSpecialisations();

} // SpecialisationChooser
