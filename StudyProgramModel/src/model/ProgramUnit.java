/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see model.ModelPackage#getProgramUnit()
 * @model
 * @generated
 */
public interface ProgramUnit extends EObject {
} // ProgramUnit
