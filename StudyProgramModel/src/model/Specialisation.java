/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.Specialisation#getName <em>Name</em>}</li>
 *   <li>{@link model.Specialisation#getProgramUnits <em>Program Units</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getSpecialisation()
 * @model
 * @generated
 */
public interface Specialisation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see model.ModelPackage#getSpecialisation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link model.Specialisation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Program Units</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program Units</em>' containment reference.
	 * @see #setProgramUnits(ProgramUnit)
	 * @see model.ModelPackage#getSpecialisation_ProgramUnits()
	 * @model containment="true"
	 * @generated
	 */
	ProgramUnit getProgramUnits();

	/**
	 * Sets the value of the '{@link model.Specialisation#getProgramUnits <em>Program Units</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program Units</em>' containment reference.
	 * @see #getProgramUnits()
	 * @generated
	 */
	void setProgramUnits(ProgramUnit value);

} // Specialisation
