/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation Catalogue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.SpecialisationCatalogue#getSpecialisations <em>Specialisations</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getSpecialisationCatalogue()
 * @model
 * @generated
 */
public interface SpecialisationCatalogue extends EObject {
	/**
	 * Returns the value of the '<em><b>Specialisations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisations</em>' containment reference.
	 * @see #setSpecialisations(Specialisation)
	 * @see model.ModelPackage#getSpecialisationCatalogue_Specialisations()
	 * @model containment="true"
	 * @generated
	 */
	Specialisation getSpecialisations();

	/**
	 * Sets the value of the '{@link model.SpecialisationCatalogue#getSpecialisations <em>Specialisations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialisations</em>' containment reference.
	 * @see #getSpecialisations()
	 * @generated
	 */
	void setSpecialisations(Specialisation value);

} // SpecialisationCatalogue
