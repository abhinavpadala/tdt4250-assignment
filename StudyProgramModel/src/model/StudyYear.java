/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Year</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.StudyYear#getAutumn <em>Autumn</em>}</li>
 *   <li>{@link model.StudyYear#getSpring <em>Spring</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getStudyYear()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='autumnSemesterType springSemesterType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL autumnSemesterType='self.autumn.semesterType=SemesterType::Autumn' springSemesterType='self.spring.semesterType=SemesterType::Spring'"
 * @generated
 */
public interface StudyYear extends ProgramUnit {
	/**
	 * Returns the value of the '<em><b>Autumn</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Autumn</em>' containment reference.
	 * @see #setAutumn(Semester)
	 * @see model.ModelPackage#getStudyYear_Autumn()
	 * @model containment="true"
	 * @generated
	 */
	Semester getAutumn();

	/**
	 * Sets the value of the '{@link model.StudyYear#getAutumn <em>Autumn</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Autumn</em>' containment reference.
	 * @see #getAutumn()
	 * @generated
	 */
	void setAutumn(Semester value);

	/**
	 * Returns the value of the '<em><b>Spring</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Spring</em>' containment reference.
	 * @see #setSpring(Semester)
	 * @see model.ModelPackage#getStudyYear_Spring()
	 * @model containment="true"
	 * @generated
	 */
	Semester getSpring();

	/**
	 * Sets the value of the '{@link model.StudyYear#getSpring <em>Spring</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Spring</em>' containment reference.
	 * @see #getSpring()
	 * @generated
	 */
	void setSpring(Semester value);

} // StudyYear
