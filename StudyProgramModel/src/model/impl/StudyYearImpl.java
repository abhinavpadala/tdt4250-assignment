/**
 */
package model.impl;

import model.ModelPackage;
import model.Semester;
import model.StudyYear;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Year</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.StudyYearImpl#getAutumn <em>Autumn</em>}</li>
 *   <li>{@link model.impl.StudyYearImpl#getSpring <em>Spring</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyYearImpl extends ProgramUnitImpl implements StudyYear {
	/**
	 * The cached value of the '{@link #getAutumn() <em>Autumn</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutumn()
	 * @generated
	 * @ordered
	 */
	protected Semester autumn;

	/**
	 * The cached value of the '{@link #getSpring() <em>Spring</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpring()
	 * @generated
	 * @ordered
	 */
	protected Semester spring;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyYearImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.STUDY_YEAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester getAutumn() {
		return autumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAutumn(Semester newAutumn, NotificationChain msgs) {
		Semester oldAutumn = autumn;
		autumn = newAutumn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.STUDY_YEAR__AUTUMN, oldAutumn, newAutumn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAutumn(Semester newAutumn) {
		if (newAutumn != autumn) {
			NotificationChain msgs = null;
			if (autumn != null)
				msgs = ((InternalEObject)autumn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.STUDY_YEAR__AUTUMN, null, msgs);
			if (newAutumn != null)
				msgs = ((InternalEObject)newAutumn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.STUDY_YEAR__AUTUMN, null, msgs);
			msgs = basicSetAutumn(newAutumn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.STUDY_YEAR__AUTUMN, newAutumn, newAutumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester getSpring() {
		return spring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpring(Semester newSpring, NotificationChain msgs) {
		Semester oldSpring = spring;
		spring = newSpring;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.STUDY_YEAR__SPRING, oldSpring, newSpring);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpring(Semester newSpring) {
		if (newSpring != spring) {
			NotificationChain msgs = null;
			if (spring != null)
				msgs = ((InternalEObject)spring).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.STUDY_YEAR__SPRING, null, msgs);
			if (newSpring != null)
				msgs = ((InternalEObject)newSpring).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.STUDY_YEAR__SPRING, null, msgs);
			msgs = basicSetSpring(newSpring, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.STUDY_YEAR__SPRING, newSpring, newSpring));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.STUDY_YEAR__AUTUMN:
				return basicSetAutumn(null, msgs);
			case ModelPackage.STUDY_YEAR__SPRING:
				return basicSetSpring(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.STUDY_YEAR__AUTUMN:
				return getAutumn();
			case ModelPackage.STUDY_YEAR__SPRING:
				return getSpring();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.STUDY_YEAR__AUTUMN:
				setAutumn((Semester)newValue);
				return;
			case ModelPackage.STUDY_YEAR__SPRING:
				setSpring((Semester)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.STUDY_YEAR__AUTUMN:
				setAutumn((Semester)null);
				return;
			case ModelPackage.STUDY_YEAR__SPRING:
				setSpring((Semester)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.STUDY_YEAR__AUTUMN:
				return autumn != null;
			case ModelPackage.STUDY_YEAR__SPRING:
				return spring != null;
		}
		return super.eIsSet(featureID);
	}

} //StudyYearImpl
