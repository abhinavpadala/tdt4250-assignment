/**
 */
package model.impl;

import model.CourseCatalogue;
import model.ModelPackage;
import model.Program;
import model.SpecialisationCatalogue;
import model.University;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.UniversityImpl#getCatalogue <em>Catalogue</em>}</li>
 *   <li>{@link model.impl.UniversityImpl#getPrograms <em>Programs</em>}</li>
 *   <li>{@link model.impl.UniversityImpl#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversityImpl extends MinimalEObjectImpl.Container implements University {
	/**
	 * The cached value of the '{@link #getCatalogue() <em>Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCatalogue()
	 * @generated
	 * @ordered
	 */
	protected CourseCatalogue catalogue;

	/**
	 * The cached value of the '{@link #getPrograms() <em>Programs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrograms()
	 * @generated
	 * @ordered
	 */
	protected Program programs;

	/**
	 * The cached value of the '{@link #getSpecialisationCatalogue() <em>Specialisation Catalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialisationCatalogue()
	 * @generated
	 * @ordered
	 */
	protected SpecialisationCatalogue specialisationCatalogue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseCatalogue getCatalogue() {
		return catalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatalogue(CourseCatalogue newCatalogue, NotificationChain msgs) {
		CourseCatalogue oldCatalogue = catalogue;
		catalogue = newCatalogue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.UNIVERSITY__CATALOGUE, oldCatalogue, newCatalogue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCatalogue(CourseCatalogue newCatalogue) {
		if (newCatalogue != catalogue) {
			NotificationChain msgs = null;
			if (catalogue != null)
				msgs = ((InternalEObject)catalogue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.UNIVERSITY__CATALOGUE, null, msgs);
			if (newCatalogue != null)
				msgs = ((InternalEObject)newCatalogue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.UNIVERSITY__CATALOGUE, null, msgs);
			msgs = basicSetCatalogue(newCatalogue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.UNIVERSITY__CATALOGUE, newCatalogue, newCatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Program getPrograms() {
		return programs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrograms(Program newPrograms, NotificationChain msgs) {
		Program oldPrograms = programs;
		programs = newPrograms;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.UNIVERSITY__PROGRAMS, oldPrograms, newPrograms);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPrograms(Program newPrograms) {
		if (newPrograms != programs) {
			NotificationChain msgs = null;
			if (programs != null)
				msgs = ((InternalEObject)programs).eInverseRemove(this, ModelPackage.PROGRAM__UNIVERSITY, Program.class, msgs);
			if (newPrograms != null)
				msgs = ((InternalEObject)newPrograms).eInverseAdd(this, ModelPackage.PROGRAM__UNIVERSITY, Program.class, msgs);
			msgs = basicSetPrograms(newPrograms, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.UNIVERSITY__PROGRAMS, newPrograms, newPrograms));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SpecialisationCatalogue getSpecialisationCatalogue() {
		if (specialisationCatalogue != null && specialisationCatalogue.eIsProxy()) {
			InternalEObject oldSpecialisationCatalogue = (InternalEObject)specialisationCatalogue;
			specialisationCatalogue = (SpecialisationCatalogue)eResolveProxy(oldSpecialisationCatalogue);
			if (specialisationCatalogue != oldSpecialisationCatalogue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, oldSpecialisationCatalogue, specialisationCatalogue));
			}
		}
		return specialisationCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecialisationCatalogue basicGetSpecialisationCatalogue() {
		return specialisationCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialisationCatalogue(SpecialisationCatalogue newSpecialisationCatalogue) {
		SpecialisationCatalogue oldSpecialisationCatalogue = specialisationCatalogue;
		specialisationCatalogue = newSpecialisationCatalogue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, oldSpecialisationCatalogue, specialisationCatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__PROGRAMS:
				if (programs != null)
					msgs = ((InternalEObject)programs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.UNIVERSITY__PROGRAMS, null, msgs);
				return basicSetPrograms((Program)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__CATALOGUE:
				return basicSetCatalogue(null, msgs);
			case ModelPackage.UNIVERSITY__PROGRAMS:
				return basicSetPrograms(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__CATALOGUE:
				return getCatalogue();
			case ModelPackage.UNIVERSITY__PROGRAMS:
				return getPrograms();
			case ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
				if (resolve) return getSpecialisationCatalogue();
				return basicGetSpecialisationCatalogue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__CATALOGUE:
				setCatalogue((CourseCatalogue)newValue);
				return;
			case ModelPackage.UNIVERSITY__PROGRAMS:
				setPrograms((Program)newValue);
				return;
			case ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
				setSpecialisationCatalogue((SpecialisationCatalogue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__CATALOGUE:
				setCatalogue((CourseCatalogue)null);
				return;
			case ModelPackage.UNIVERSITY__PROGRAMS:
				setPrograms((Program)null);
				return;
			case ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
				setSpecialisationCatalogue((SpecialisationCatalogue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.UNIVERSITY__CATALOGUE:
				return catalogue != null;
			case ModelPackage.UNIVERSITY__PROGRAMS:
				return programs != null;
			case ModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
				return specialisationCatalogue != null;
		}
		return super.eIsSet(featureID);
	}

} //UniversityImpl
