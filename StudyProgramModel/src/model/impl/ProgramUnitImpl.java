/**
 */
package model.impl;

import model.ModelPackage;
import model.ProgramUnit;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProgramUnitImpl extends MinimalEObjectImpl.Container implements ProgramUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.PROGRAM_UNIT;
	}

} //ProgramUnitImpl
