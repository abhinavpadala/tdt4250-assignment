/**
 */
package model.impl;

import java.util.Collection;

import model.ModelPackage;
import model.Specialisation;
import model.SpecialisationChooser;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specialisation Chooser</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.SpecialisationChooserImpl#getAvailableSpecialisations <em>Available Specialisations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpecialisationChooserImpl extends ProgramUnitImpl implements SpecialisationChooser {
	/**
	 * The cached value of the '{@link #getAvailableSpecialisations() <em>Available Specialisations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableSpecialisations()
	 * @generated
	 * @ordered
	 */
	protected EList<Specialisation> availableSpecialisations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialisationChooserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SPECIALISATION_CHOOSER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialisation> getAvailableSpecialisations() {
		if (availableSpecialisations == null) {
			availableSpecialisations = new EObjectResolvingEList<Specialisation>(Specialisation.class, this, ModelPackage.SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS);
		}
		return availableSpecialisations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS:
				return getAvailableSpecialisations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS:
				getAvailableSpecialisations().clear();
				getAvailableSpecialisations().addAll((Collection<? extends Specialisation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS:
				getAvailableSpecialisations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS:
				return availableSpecialisations != null && !availableSpecialisations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SpecialisationChooserImpl
