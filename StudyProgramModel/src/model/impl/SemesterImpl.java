/**
 */
package model.impl;

import model.Course;
import model.ElectiveGroup;
import model.ModelPackage;
import model.Semester;
import model.SemesterType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.SemesterImpl#getSemesterType <em>Semester Type</em>}</li>
 *   <li>{@link model.impl.SemesterImpl#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link model.impl.SemesterImpl#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link model.impl.SemesterImpl#getElectiveCourseGroups <em>Elective Course Groups</em>}</li>
 *   <li>{@link model.impl.SemesterImpl#getAllCourses <em>All Courses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The default value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterType SEMESTER_TYPE_EDEFAULT = SemesterType.SPRING;

	/**
	 * The cached value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected SemesterType semesterType = SEMESTER_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMandatoryCourses() <em>Mandatory Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatoryCourses()
	 * @generated
	 * @ordered
	 */
	protected Course mandatoryCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourses() <em>Elective Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourses()
	 * @generated
	 * @ordered
	 */
	protected Course electiveCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourseGroups() <em>Elective Course Groups</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourseGroups()
	 * @generated
	 * @ordered
	 */
	protected ElectiveGroup electiveCourseGroups;

	/**
	 * The cached value of the '{@link #getAllCourses() <em>All Courses</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCourses()
	 * @generated
	 * @ordered
	 */
	protected Course allCourses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SemesterType getSemesterType() {
		return semesterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemesterType(SemesterType newSemesterType) {
		SemesterType oldSemesterType = semesterType;
		semesterType = newSemesterType == null ? SEMESTER_TYPE_EDEFAULT : newSemesterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__SEMESTER_TYPE, oldSemesterType, semesterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course getMandatoryCourses() {
		if (mandatoryCourses != null && mandatoryCourses.eIsProxy()) {
			InternalEObject oldMandatoryCourses = (InternalEObject)mandatoryCourses;
			mandatoryCourses = (Course)eResolveProxy(oldMandatoryCourses);
			if (mandatoryCourses != oldMandatoryCourses) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.SEMESTER__MANDATORY_COURSES, oldMandatoryCourses, mandatoryCourses));
			}
		}
		return mandatoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetMandatoryCourses() {
		return mandatoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMandatoryCourses(Course newMandatoryCourses) {
		Course oldMandatoryCourses = mandatoryCourses;
		mandatoryCourses = newMandatoryCourses;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__MANDATORY_COURSES, oldMandatoryCourses, mandatoryCourses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course getElectiveCourses() {
		if (electiveCourses != null && electiveCourses.eIsProxy()) {
			InternalEObject oldElectiveCourses = (InternalEObject)electiveCourses;
			electiveCourses = (Course)eResolveProxy(oldElectiveCourses);
			if (electiveCourses != oldElectiveCourses) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.SEMESTER__ELECTIVE_COURSES, oldElectiveCourses, electiveCourses));
			}
		}
		return electiveCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetElectiveCourses() {
		return electiveCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setElectiveCourses(Course newElectiveCourses) {
		Course oldElectiveCourses = electiveCourses;
		electiveCourses = newElectiveCourses;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__ELECTIVE_COURSES, oldElectiveCourses, electiveCourses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElectiveGroup getElectiveCourseGroups() {
		return electiveCourseGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElectiveCourseGroups(ElectiveGroup newElectiveCourseGroups, NotificationChain msgs) {
		ElectiveGroup oldElectiveCourseGroups = electiveCourseGroups;
		electiveCourseGroups = newElectiveCourseGroups;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS, oldElectiveCourseGroups, newElectiveCourseGroups);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setElectiveCourseGroups(ElectiveGroup newElectiveCourseGroups) {
		if (newElectiveCourseGroups != electiveCourseGroups) {
			NotificationChain msgs = null;
			if (electiveCourseGroups != null)
				msgs = ((InternalEObject)electiveCourseGroups).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS, null, msgs);
			if (newElectiveCourseGroups != null)
				msgs = ((InternalEObject)newElectiveCourseGroups).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS, null, msgs);
			msgs = basicSetElectiveCourseGroups(newElectiveCourseGroups, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS, newElectiveCourseGroups, newElectiveCourseGroups));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course getAllCourses() {
		if (allCourses != null && allCourses.eIsProxy()) {
			InternalEObject oldAllCourses = (InternalEObject)allCourses;
			allCourses = (Course)eResolveProxy(oldAllCourses);
			if (allCourses != oldAllCourses) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.SEMESTER__ALL_COURSES, oldAllCourses, allCourses));
			}
		}
		return allCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetAllCourses() {
		return allCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAllCourses(Course newAllCourses) {
		Course oldAllCourses = allCourses;
		allCourses = newAllCourses;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SEMESTER__ALL_COURSES, oldAllCourses, allCourses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
				return basicSetElectiveCourseGroups(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SEMESTER__SEMESTER_TYPE:
				return getSemesterType();
			case ModelPackage.SEMESTER__MANDATORY_COURSES:
				if (resolve) return getMandatoryCourses();
				return basicGetMandatoryCourses();
			case ModelPackage.SEMESTER__ELECTIVE_COURSES:
				if (resolve) return getElectiveCourses();
				return basicGetElectiveCourses();
			case ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
				return getElectiveCourseGroups();
			case ModelPackage.SEMESTER__ALL_COURSES:
				if (resolve) return getAllCourses();
				return basicGetAllCourses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SEMESTER__SEMESTER_TYPE:
				setSemesterType((SemesterType)newValue);
				return;
			case ModelPackage.SEMESTER__MANDATORY_COURSES:
				setMandatoryCourses((Course)newValue);
				return;
			case ModelPackage.SEMESTER__ELECTIVE_COURSES:
				setElectiveCourses((Course)newValue);
				return;
			case ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
				setElectiveCourseGroups((ElectiveGroup)newValue);
				return;
			case ModelPackage.SEMESTER__ALL_COURSES:
				setAllCourses((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SEMESTER__SEMESTER_TYPE:
				setSemesterType(SEMESTER_TYPE_EDEFAULT);
				return;
			case ModelPackage.SEMESTER__MANDATORY_COURSES:
				setMandatoryCourses((Course)null);
				return;
			case ModelPackage.SEMESTER__ELECTIVE_COURSES:
				setElectiveCourses((Course)null);
				return;
			case ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
				setElectiveCourseGroups((ElectiveGroup)null);
				return;
			case ModelPackage.SEMESTER__ALL_COURSES:
				setAllCourses((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SEMESTER__SEMESTER_TYPE:
				return semesterType != SEMESTER_TYPE_EDEFAULT;
			case ModelPackage.SEMESTER__MANDATORY_COURSES:
				return mandatoryCourses != null;
			case ModelPackage.SEMESTER__ELECTIVE_COURSES:
				return electiveCourses != null;
			case ModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
				return electiveCourseGroups != null;
			case ModelPackage.SEMESTER__ALL_COURSES:
				return allCourses != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semesterType: ");
		result.append(semesterType);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
