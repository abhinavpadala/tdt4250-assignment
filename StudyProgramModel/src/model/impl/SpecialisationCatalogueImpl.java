/**
 */
package model.impl;

import model.ModelPackage;
import model.Specialisation;
import model.SpecialisationCatalogue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specialisation Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.SpecialisationCatalogueImpl#getSpecialisations <em>Specialisations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpecialisationCatalogueImpl extends MinimalEObjectImpl.Container implements SpecialisationCatalogue {
	/**
	 * The cached value of the '{@link #getSpecialisations() <em>Specialisations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialisations()
	 * @generated
	 * @ordered
	 */
	protected Specialisation specialisations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialisationCatalogueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SPECIALISATION_CATALOGUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Specialisation getSpecialisations() {
		return specialisations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpecialisations(Specialisation newSpecialisations, NotificationChain msgs) {
		Specialisation oldSpecialisations = specialisations;
		specialisations = newSpecialisations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS, oldSpecialisations, newSpecialisations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialisations(Specialisation newSpecialisations) {
		if (newSpecialisations != specialisations) {
			NotificationChain msgs = null;
			if (specialisations != null)
				msgs = ((InternalEObject)specialisations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS, null, msgs);
			if (newSpecialisations != null)
				msgs = ((InternalEObject)newSpecialisations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS, null, msgs);
			msgs = basicSetSpecialisations(newSpecialisations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS, newSpecialisations, newSpecialisations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS:
				return basicSetSpecialisations(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS:
				return getSpecialisations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS:
				setSpecialisations((Specialisation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS:
				setSpecialisations((Specialisation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SPECIALISATION_CATALOGUE__SPECIALISATIONS:
				return specialisations != null;
		}
		return super.eIsSet(featureID);
	}

} //SpecialisationCatalogueImpl
