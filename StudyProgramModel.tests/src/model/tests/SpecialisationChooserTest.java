/**
 */
package model.tests;

import junit.textui.TestRunner;

import model.ModelFactory;
import model.SpecialisationChooser;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Specialisation Chooser</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpecialisationChooserTest extends ProgramUnitTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SpecialisationChooserTest.class);
	}

	/**
	 * Constructs a new Specialisation Chooser test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecialisationChooserTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Specialisation Chooser test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SpecialisationChooser getFixture() {
		return (SpecialisationChooser)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelFactory.eINSTANCE.createSpecialisationChooser());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SpecialisationChooserTest
