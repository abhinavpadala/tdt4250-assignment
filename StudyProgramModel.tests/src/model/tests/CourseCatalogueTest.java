/**
 */
package model.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import model.CourseCatalogue;
import model.ModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Course Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CourseCatalogueTest extends TestCase {

	/**
	 * The fixture for this Course Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseCatalogue fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CourseCatalogueTest.class);
	}

	/**
	 * Constructs a new Course Catalogue test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseCatalogueTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Course Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CourseCatalogue fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Course Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseCatalogue getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelFactory.eINSTANCE.createCourseCatalogue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CourseCatalogueTest
