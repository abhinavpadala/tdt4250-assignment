/**
 */
package model.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import model.ModelFactory;
import model.SpecialisationCatalogue;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Specialisation Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpecialisationCatalogueTest extends TestCase {

	/**
	 * The fixture for this Specialisation Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialisationCatalogue fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SpecialisationCatalogueTest.class);
	}

	/**
	 * Constructs a new Specialisation Catalogue test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecialisationCatalogueTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Specialisation Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(SpecialisationCatalogue fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Specialisation Catalogue test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialisationCatalogue getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelFactory.eINSTANCE.createSpecialisationCatalogue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SpecialisationCatalogueTest
